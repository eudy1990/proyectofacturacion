﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacturaReporte
{
    public partial class frmReportViewer1 : Form
    {
        public frmReportViewer1()
        {
            InitializeComponent();
        }

        private void frmReportViewer1_Load(object sender, EventArgs e)
        {

            //this.reportViewer1.RefreshReport();
        }
        public void configurarReporte(List<FacturaDetalle> facturaDetalles, Factura factura, Cliente cliente) {

            //Microsoft.Reporting.WinForms.ReportDataSource
            this.reportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1",facturaDetalles));
            this.reportViewer1.LocalReport.ReportPath = AppDomain.CurrentDomain.BaseDirectory+ "Factura.rdlc";

            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpNombreCliente",cliente.Nombre+","+cliente.Apellido));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpNoFactura", factura.ID.ToString()));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpTotal", factura.Total.ToString()));

            this.reportViewer1.RefreshReport();
        }


    }
}
