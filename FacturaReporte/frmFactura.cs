﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacturaReporte
{
    public partial class frmFactura : Form
    {

        private List<Cliente> clientes = new List<Cliente>();
        private List<Producto> productos = new List<Producto>();
        private FacturaDetalle productoSeleccionado = new FacturaDetalle();
        private List<FacturaDetalle> facturaDetalles = new List<FacturaDetalle>();
        private Factura factura = new Factura();
        private Cliente cliente = new Cliente();

        public frmFactura()
        {
            InitializeComponent();
            this.llenarListaCliente();
            this.mostarClienteDGV();
            this.llenarListaProducto();
            this.mostarProductoDGV();
        }

        public void llenarListaCliente() {
            this.clientes.Add(new Cliente(1,"Juan","Perez"));
            this.clientes.Add(new Cliente(2,"Jasiel", "Ramirez"));
            this.clientes.Add(new Cliente(3,"Cristian", "Ovalle"));
        }
        public void llenarListaProducto()
        {
            this.productos.Add(new Producto(1, "Arroz",50));
            this.productos.Add(new Producto(2, "Mochila", 20.60f));
            this.productos.Add(new Producto(3, "Yuca", 12));
        }

        public void mostarClienteDGV() {
            this.dataGridView1.DataSource = this.clientes;
        }
        public void mostarProductoDGV()
        {
            this.dataGridView2.DataSource = this.productos;
        }
        public void mostrarDetalleFactura() {

            this.dataGridView3.DataSource = this.facturaDetalles.ToList() ;
        }

        public void mostrarTotal() {
            this.factura.Total = this.facturaDetalles.Sum(x => x.SubTotal);
            /*this.factura.Total = 0;
            foreach (var item in facturaDetalles)
            {
                this.factura.Total += item.SubTotal;
            }*/
            this.label5.Text = this.factura.Total.ToString();
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void frmFactura_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //Producto seleccionado por el usuario
            var auxProductoSeleccionado=(Producto)this.dataGridView2.CurrentRow.DataBoundItem;

            //Asignamos los datos del producto a los textBox
            this.textBox3.Text = auxProductoSeleccionado.Nombre;
            this.textBox5.Text = auxProductoSeleccionado.Precio.ToString();
            this.textBox4.Text = "1";
            this.textBox6.Text = auxProductoSeleccionado.Precio.ToString();

            ///Creamos un item para factura detalle
            this.productoSeleccionado = new FacturaDetalle(0,
                0,
                auxProductoSeleccionado.ID,
                auxProductoSeleccionado.Nombre,
                auxProductoSeleccionado.Precio,
                1,
                auxProductoSeleccionado.Precio
                );

        }

        private void textBox4_KeyUp(object sender, KeyEventArgs e)
        {
            float cantidad = 1;
            float.TryParse(this.textBox4.Text, out cantidad);

            this.productoSeleccionado.Cantidad = cantidad;
            this.productoSeleccionado.SubTotal = this.productoSeleccionado.Precio * this.productoSeleccionado.Cantidad;

            this.textBox6.Text = this.productoSeleccionado.SubTotal.ToString() ;

        }

        private void iconButton3_Click(object sender, EventArgs e)
        {
            frmReportViewer1 frmReportViewer1 = new frmReportViewer1();
            frmReportViewer1.configurarReporte(this.facturaDetalles,this.factura,this.cliente);
            frmReportViewer1.Show();
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void iconButton1_Click(object sender, EventArgs e)
        {
            this.facturaDetalles.Add(productoSeleccionado);
            this.mostrarDetalleFactura();
            this.mostrarTotal();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.cliente = (Cliente)this.dataGridView1.CurrentRow.DataBoundItem;
        }
    }
}
