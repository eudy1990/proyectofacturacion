﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacturaReporte
{
    public partial class frmReportViewer : Form
    {
        public frmReportViewer()
        {
            InitializeComponent();
        }

        private void frmReportViewer_Load(object sender, EventArgs e)
        {
            List<FacturaDetalle> facturaDetalles = new List<FacturaDetalle>();
            facturaDetalles.Add(new FacturaDetalle(0,0,0,"nombre",10,11,12));
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource = new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1",facturaDetalles);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
            this.reportViewer1.LocalReport.ReportPath = AppDomain.CurrentDomain.BaseDirectory+ "Report1.rdlc";
            this.reportViewer1.RefreshReport();
        }
    }
}
